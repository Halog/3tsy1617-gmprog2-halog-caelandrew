﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpMovement : AsteroidMovement {

	// Use this for initialization
	void Start () 
	{
		speed = 4f;
		spinSpeed = speed;
		spinSpeed *= 20f;
		Destroy (gameObject, 30f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		spinSpecial();
		movement ();
		destroyOffScreen ();
	}

	public void spinSpecial()
	{
		transform.Rotate(Vector3.up, spinSpeed * Time.deltaTime);
		transform.Rotate(Vector3.left, spinSpeed * Time.deltaTime);
	}
}
