﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public static int uiScore;
	public static int uiLives;

	Text text;
	// Use this for initialization
	void Awake () 
	{
		text = GetComponent <Text> ();
		uiScore = 0;
		uiLives = 3;
	}
	
	// Update is called once per frame
	void Update () 
	{
		text.text = "Score:" + uiScore;	
		text.text = "Lives:" + uiLives;
	}
}
