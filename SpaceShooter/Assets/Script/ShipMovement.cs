﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour {

	public int lives = 3;
	public float speed = 10;
	public float boundaryX = 6.5f;
	public float boundaryY = 5;
	public GameObject bullet;
	private bool altFire = true;
	private float offset = 0.14f;
	private Renderer rend;
	private Renderer rendBooster;
	private bool invulnerable = false;
	//0.14x offset of cannon from center
	void Start () 
	{
		rend = GetComponent<Renderer> ();
	}

	// Update is called once per frame
	void Update () 
	{
		constrain();
		movement();
		shoot();
		test();
	}
	void movement() 
	{
		//IF statements purely for aesthetics reasons (ship yaw). keyboard input only
		if (Input.GetKey ("a"))
			transform.localRotation = Quaternion.Euler(0f,Mathf.LerpAngle(transform.localRotation.eulerAngles.y, 40 , Time.deltaTime * 10f), 0f);
		else if (Input.GetKey ("d"))
			transform.localRotation = Quaternion.Euler(0f,Mathf.LerpAngle(transform.localRotation.eulerAngles.y, -40, Time.deltaTime * 10f), 0f);
		else
			transform.localRotation = Quaternion.Euler(0f,Mathf.LerpAngle(transform.localRotation.eulerAngles.y, 0, Time.deltaTime * 10f), 0f);
		//Actual movement algorithm. Get axis for controller compatibility.
		transform.Translate (Input.GetAxisRaw("Horizontal")*speed*Time.deltaTime,0,0,Space.World);
		transform.Translate (0,Input.GetAxisRaw("Vertical")*speed*Time.deltaTime,0);
	}
	void shoot()
	{
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			//Blasters fire with an alternating pattern since ship model has 2 cannons. Also solves fire rate problems unless user uses a macro.
			if (altFire) 
			{
				Instantiate (bullet, transform.position - new Vector3 (offset, -0.5f, 0), new Quaternion(0,0,0,0));
				altFire = false;	
			}
			else 
			{
				Instantiate (bullet, transform.position - new Vector3(-offset, -0.5f,0), new Quaternion(0,0,0,0));
				altFire = true;
			}

		}
	}
	void constrain()
	{
		if (transform.position.x > boundaryX)
			transform.position = new Vector3(boundaryX,transform.position.y,transform.position.z);
		if (transform.position.x < -(boundaryX))
			transform.position = new Vector3(-(boundaryX),transform.position.y,transform.position.z);
		if (transform.position.y > boundaryY)
			transform.position = new Vector3(transform.position.x,boundaryY,transform.position.z);
		if (transform.position.y < -(boundaryY))
			transform.position = new Vector3(transform.position.x,-(boundaryY),transform.position.z);
	}
	void test()
	{
		//Mathf.Clamp (transform.rotation,-30,30);
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "asteroid" && !invulnerable)
		{
			Destroy (other.gameObject);
			UserInterface.uiLives--;
			StartCoroutine (iFrames ());
		}
		if (other.tag == "powerup")
		{
			Destroy (other.gameObject);
			UserInterface.uiLives++;
		}
	}
	IEnumerator iFrames()
	{		
		invulnerable = true;
		rend.enabled = false;
		for (int i = 0; i < 5; i++) 
		{
			yield return new WaitForSeconds (0.15f);
			rend.enabled = false;
			yield return new WaitForSeconds (0.15f);
			rend.enabled = true;
		}
		invulnerable = false;
	}
}
