﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

	public float speed;
	// Use this for initialization
	void Start () 
	{
		
	}
	void Awake()
	{
		Destroy (gameObject,  2.0f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Translate(0,speed*Time.deltaTime,0);
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "asteroid")
		{
			Destroy (other.gameObject);
			Destroy (gameObject);
			UserInterface.uiScore += 100;
		}
	}
}
