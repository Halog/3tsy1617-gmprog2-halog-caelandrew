﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour {
	//6.5 screen bounds
	private float timer;
	public float spawnPointMaxRange;
	public float spawnPointMinRange;
	public float powerSpawnChance;
	public float spawnInterval;
	private int powerSpawnRandomizer;
	public GameObject objectSpawned;
	public GameObject powerSpawned;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		timer += Time.deltaTime;
		if (timer > spawnInterval) 
		{
			Instantiate (objectSpawned, transform.position - new Vector3(Random.Range(spawnPointMinRange,spawnPointMaxRange),0,0), new Quaternion(0,0,0,0));
			spawnInterval = Random.Range (0, 5);
			powerSpawnRandomizer = Random.Range (0, 99);
			if(powerSpawnRandomizer < powerSpawnChance)
				Instantiate (powerSpawned, transform.position - new Vector3(Random.Range(spawnPointMinRange,spawnPointMaxRange),0,0), new Quaternion(0,0,0,0));
			timer = 0;
		}
	}
}
