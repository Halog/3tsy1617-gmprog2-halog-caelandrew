﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinLose : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(UserInterface.uiLives < 0)
			SceneManager.LoadScene("GameOver");
		if(UserInterface.uiScore > 1000)
			SceneManager.LoadScene("GameOver");		
	}
}
