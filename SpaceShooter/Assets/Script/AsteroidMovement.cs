﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMovement : MonoBehaviour {

	public float speed;
	public float spinSpeed;
	// 10 speed = 200 spinspeed
	void Start () 
	{
		speed = Random.Range (1, 10);
		spinSpeed = speed;
		spinSpeed *= 20f;
		Destroy (gameObject, 30f);
	}
	void Update () 
	{
		spin ();
		movement ();
		destroyOffScreen ();
	}
	public void spin()
	{
		transform.Rotate(Vector3.left, spinSpeed * Time.deltaTime);
	}
	public void movement()
	{
		transform.Translate(0, -speed * Time.deltaTime,0, Space.World);
	}
	public void destroyOffScreen()
	{
		if (transform.position.y < -10)
			Destroy (gameObject);
	}




}
