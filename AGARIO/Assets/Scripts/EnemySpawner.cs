﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	//Modifiable Values
	public int startingEnemies;
	public int maxEnemies = 5;
	//Instantiations
	public GameObject objectSpawned;
	Collider[] enemies;

	void Start () 
	{
		for (int i = 0; i < startingEnemies; i++)
			Spawn ();
		InvokeRepeating ("checkRespawn", 0, 1);
	}

	void CheckEnemyNumbers()
	{
		enemies = Physics.OverlapSphere (transform.position, Mathf.Infinity, 1<<8);
	}

	void CheckConditionsToRespawn()
	{
		CheckEnemyNumbers ();
		if (enemies.Length <= maxEnemies)
			Spawn ();
	}

	void Spawn()
	{		
		GameObject enemy = Instantiate (objectSpawned, transform.position - new Vector3 (Random.Range (-68.4f, 68.4f), Random.Range (-68.4f, 68.4f), 0), new Quaternion (0, 0, 0, 0));
	}	
}
