﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

	public GameObject objFood;
	Collider [] foodArray;
	public int foodCount;


	void Start () 
	{
		for(int i = 0; i < 30; i++) 
			Instantiate(objFood, transform.position - new Vector3(Random.Range(-68.4f, 68.4f),Random.Range(-68.4f, 68.4f),0), new Quaternion(0,0,0,0));
		InvokeRepeating("foodLoop", 0, 1);
		//StartCoroutine (FoodSpawner());
	}
		
	void FoodLoop()
	{
		CheckFoodCount ();
		SpawnFood();
		foodCount = foodArray.Length;
	}
	void CheckFoodCount()
	{
		foodArray = Physics.OverlapSphere (transform.position, Mathf.Infinity, 1<<9);
	}
	void SpawnFood()
	{
		if(foodArray.Length <= 100)
			Instantiate(objFood, transform.position - new Vector3(Random.Range(-68.4f, 68.4f),Random.Range(-68.4f, 68.4f),0), new Quaternion(0,0,0,0));
	}

}
