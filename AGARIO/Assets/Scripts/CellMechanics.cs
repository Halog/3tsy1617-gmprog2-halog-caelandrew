﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellMechanics : MonoBehaviour {

	//Modifiable Values
	public float speed = 5;
	public float massRetentionPercentage = 20;
	public float percentSizeToEat = 85;
	public float foodPotency = 0.05f;
	public int foodScore = 1;
	//Instantiations
	public int score = 0;
	Vector3 targetPos;
	CellMechanics cellMechanics;

	void Update () 
	{
		SizeControl();
		//Enemy has seperate movement script
		if(gameObject.tag != "enemy")
			Movement ();
		Clamp ();
	}

	void SizeControl ()
	{
		if (Input.GetKey (KeyCode.Q) && gameObject.tag != "enemy") 
		{
			AddMass (0.1f);
		}
			
		if (Input.GetKey (KeyCode.E) && gameObject.tag != "enemy")
		{
			LessMass (0.1f);
		}
	}

	void Movement ()
	{
		Clamp ();
		targetPos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		targetPos = new Vector3 (targetPos.x, targetPos.y, 0);

		this.transform.position = Vector3.MoveTowards (
			this.transform.position,
			targetPos,
			speed * Time.deltaTime * Mathf.Pow (transform.localScale.x, -0.44f));
	}

	void AddMass(float foodValue)
	{
		transform.localScale += new Vector3 ( foodValue, foodValue, 0);
	}

	void LessMass(float foodValue)
	{
		transform.localScale -= new Vector3 ( foodValue, foodValue, 0);
	}

	void OnTriggerEnter (Collider other)
	{
		if(other.tag == "food") 
		{
			Destroy (other.gameObject);
			AddMass(foodPotency);
			score += foodScore;
		}
		else if(other.tag != "inedible")
		{
			cellMechanics = other.GetComponent<CellMechanics> ();
			if(other.transform.localScale.x < transform.localScale.x * (percentSizeToEat * 0.01f))
			{
				AddMass (other.transform.localScale.x * (massRetentionPercentage * 0.01f) );
				Destroy (other.gameObject);
			}
		}
	}

	void Clamp()
	{
		transform.position = new Vector3 (Mathf.Clamp (transform.position.x, -68.4f, 68.4f), Mathf.Clamp (transform.position.y, -68.4f, 68.4f), 0);
	}
}
