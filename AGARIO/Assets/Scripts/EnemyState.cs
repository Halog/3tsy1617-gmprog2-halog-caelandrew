﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State
{
	Eat,
	Run,
	Patrol,
}

public class EnemyState : MonoBehaviour 
{
	//Modifiable Values
	public float maxSpeed = 5;
	public float enemyDetectionRange = 9;
	public float foodDetectionRange = 11;
	//Instantiations
	public State curState;
	Vector3 targetPos;
	Collider[] enemy = null;
	Collider[] food = null;
	Collider pursuingEnemy = null;
	bool enemyInRange = false;
	bool foodInRange = false;

	void Awake()
	{
		GeneratePoint ();
		InvokeRepeating ("scanEnemy", 0, 0.5f);
		InvokeRepeating ("scanFood", 0, 0.5f);
	}

	void Update()
	{
		switch (curState) 
		{
		case State.Run:		UpdateRun(); 	break;
		case State.Eat:		UpdateEat(); 	break;
		case State.Patrol:	UpdatePatrol(); break;
		}

		UpdatePatrol ();
	}

	void UpdateEat()
	{
		for (int i = 0; i < enemy.Length; i++) 
		{
			if (enemy [i] != null) 
			{
				if (enemyInRange && enemy [i].transform.localScale.x > transform.localScale.x * 1.20f)
				{
					pursuingEnemy = enemy [i];
					curState = State.Run;
				}
				if (enemyInRange && enemy [i].transform.localScale.x < transform.localScale.x * 0.8f) 
				{
					targetPos = enemy [i].transform.position;
					break;
				} 
				else if (foodInRange && food[0] != null) 
				{
					targetPos = food [0].transform.position;
				}
				else
					curState = State.Patrol;
			}
		}
		MoveToPoint ();
	}

	//Behavior
	//move toward nearByEnemy 
	//if the target Destroyed
	//change the state to Patrol
	//if nearByEnemy change his scale bigger than you 
	//if(nearByEnemy.localScale > transform.localScale)
	//State.Run;
	// change the state to run

	void UpdateRun()
	{
		Vector3 oppositeDirection = transform.position;
		if (!enemyInRange)
			curState = State.Patrol;
		else if (pursuingEnemy != null)
			oppositeDirection = transform.position - pursuingEnemy.transform.position;
		MoveToPoint (oppositeDirection);
	}

	//Behavior
	// check the distance if the enemy is faraway 
	//ontriggerstay - opposite direction
	// state patrol
	// if distance is close i will continue running

	void UpdatePatrol()
	{
		CheckIfPointUnreachable ();
		MoveToPoint ();
		if (transform.position.x == targetPos.x && transform.position.y == targetPos.y)
			GeneratePoint ();		
		//20% difference in size to eat other cells
		for (int i = 0; i < enemy.Length ; i++) 
		{
			if (enemy [i] != null) 
			{
				if (enemyInRange && enemy [i].transform.localScale.x > transform.localScale.x * 1.20f)
				{
					pursuingEnemy = enemy [i];
					curState = State.Run;
				}
				if (enemyInRange && enemy [i].transform.localScale.x < transform.localScale.x * 0.8f) 
				{
					curState = State.Eat;
				} 
				else if (foodInRange) 
				{
					curState = State.Eat;
				}
			}
		}
	}

	//Behavior
	// get random point where we need to gp
	// if i reach the point generate point
	//if the nearbyEnemy is smaller than you
	//change the state to Eat
	//if the nearbyEnemy is larger than you
	// change the state to Run

	void GeneratePoint()
	{		
		targetPos = new Vector3 (Random.Range(transform.position.x - 10, transform.position.x + 10), 
				                 Random.Range(transform.position.y - 10, transform.position.y + 10), 0);
	}

	void CheckIfPointUnreachable()
	{
		if (targetPos.x > 68.4 || targetPos.x < -68.4 || targetPos.y > 68.4 || targetPos.y < -68.4)
			GeneratePoint ();
	}

	void MoveToPoint()
	{
		transform.position = Vector3.MoveTowards ( transform.position, targetPos, 2 * Time.deltaTime * Mathf.Pow (transform.localScale.x - 0.5f, -0.44f));
	}

	void MoveToPoint(Vector3 direction)
	{
		transform.Translate(direction.normalized * maxSpeed * Time.deltaTime * Mathf.Pow (transform.localScale.x, -0.44f) );
	}

	void ScanEnemy () 
	{
		enemy = Physics.OverlapSphere (transform.position, enemyDetectionRange + transform.localScale.x, 1<<8);
		if (enemy.Length > 0)
			enemyInRange = true;
		else
			enemyInRange = false;	
	}

	void ScanFood () 
	{
		food = Physics.OverlapSphere (transform.position, foodDetectionRange + transform.localScale.x, 1<<9);
		if (food.Length > 0)
			foodInRange = true;
		else
			foodInRange = false;
	}
}
