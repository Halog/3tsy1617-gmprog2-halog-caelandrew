﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour {

	GameObject target;
	CellMechanics cellMechanics;

	void Start()
	{
		target = GameObject.Find("Player");
		cellMechanics = target.GetComponent<CellMechanics> ();
	}
	void LateUpdate () 
	{
		if(target != null)
			Follow ();
	}
	void Follow()
	{
		this.transform.position = new Vector3 (
			target.transform.position.x,
			target.transform.position.y,
			-9f);
		Camera.main.orthographicSize = 7f + target.transform.localScale.x ;
	}
}
