﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserInterface : MonoBehaviour 
{
	public GameObject getScoreOfObject;
	CellMechanics cellMechanics;
	Text text;

	void Awake () 
	{
		text = GetComponent <Text> ();
		cellMechanics = getScoreOfObject.GetComponent<CellMechanics> ();
	}
		
	void Update () 
	{
		if(cellMechanics != null)
			text.text = "Score:" + cellMechanics.score;	
	}
}

