﻿Shader "Unlit/JellyEnemy"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "pink" {}
		_Warp ("AmplitudeMinMax", Range(25,100)) = 50
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			float _Warp;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);

				//float2 worldPos = mul(unity_ObjectToWorld, v.vertex).xy;
				//o.vertex.x += sin(worldPos.y + _Time.w);
				//o.vertex.y += sign(o.vertex.y) * sin * _Time.w);
				//o.vertex.y += sign(o.vertex.y) * cos * _Time.w);
				//v.vertex.x += sin(v.vertex.x + _Time.w);
				//v.vertex.y += cos(v.vertex.y + _Time.w);
				v.vertex.x += sign(v.vertex.x) * sin(_Time.w)/_Warp;
     			v.vertex.y += sign(v.vertex.y) * cos(_Time.w)/_Warp;
     			o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
