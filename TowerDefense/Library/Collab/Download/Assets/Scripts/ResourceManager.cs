﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ResourceManager : MonoBehaviour {

	public int playerGold = 2000;
	public int playerLives = 10;

	public delegate void test();
	test mytest;

	void Awake()
	{
		playerGold = 2000;
	}

	public void AddGold(int goldAmount)
	{
		playerGold += goldAmount;
	}
	public void SubtractLife(int lifeAmount)
	{
		playerLives -= lifeAmount;
	}
}
