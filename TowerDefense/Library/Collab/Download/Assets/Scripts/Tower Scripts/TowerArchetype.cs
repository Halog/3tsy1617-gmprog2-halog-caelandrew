﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TowerArchetype : MonoBehaviour 
{
	//Instantiation
	public GameObject projectile;
	public Collider[] enemy = null;
	bool isShooting = false;
	//Moddable Stats - Inheritable
	public int damage;
	public BulletScript.bulletType towerAmmo;
	public int towerLevel = 1;
	public float towerRange;
	public float fireRate = 2;
	public float buildTime = 5;

	public virtual void SetTowerProperties(){}
	public virtual void StatGrowth(){}

	public void PlaceTower()
	{
		StartCoroutine (Build ());	
	}

	void ShootEnemy()
	{
		Vector3 temp = this.transform.position;
		//Offset
		temp += new Vector3 (0, 1, 0);
		GameObject bullet = Instantiate (projectile, temp, transform.rotation);
		BulletScript bulletScript = bullet.GetComponent<BulletScript> ();
		//Bullet Properties
		bulletScript.SetStats (damage, towerAmmo);
		//Set Bullet Target
		bulletScript.SetTarget (enemy [0].transform);
	}

	void ScanEnemy () 
	{
		enemy = Physics.OverlapCapsule(transform.position + new Vector3(0, 10, 0), transform.position - new Vector3(0, 10, 0), 	towerRange, 1<<8);
		if (enemy.Length > 0 && !isShooting) 
		{
			isShooting = true;
			StartCoroutine (Shoot());
		}
	}

	public IEnumerator Build()
	{
		yield return new WaitForSeconds (buildTime);
		InvokeRepeating ("ScanEnemy", 0, 0.2f);
	}

	public IEnumerator Shoot()
	{ 
		ShootEnemy ();
		yield return new WaitForSeconds(fireRate);
		isShooting = false;
	} 

}
