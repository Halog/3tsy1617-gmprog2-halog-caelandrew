﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class TowerFire : TowerArchetype 
{

	void Start()
	{
		this.SetTowerProperties ();
	}

	public override void SetTowerProperties()
	{
		damage = 10;
		towerAmmo = BulletScript.bulletType.Fire;
		towerRange = 15f;
		fireRate = 1.0f;
	}

	public override void StatGrowth()
	{
		damage += 100;
		towerRange += 5f;
		fireRate -= 0.1f;
		towerLevel += 1;
	}

}