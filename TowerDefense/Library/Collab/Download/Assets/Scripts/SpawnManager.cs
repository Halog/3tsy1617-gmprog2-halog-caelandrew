﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MonsterType
{
	Ground,
	Flying,
	Ground_Boss,
	Flying_Boss,
}

[System.Serializable]
public class MonsterData
{
	public string name;
	public GameObject[] enemyObject;
	public MonsterType eType;
	public float monsterHP;
	public float goldValue;
}

public class SpawnManager : MonoBehaviour {

	public GameObject goblin;
	public GameObject crow;
	public GameObject dragon;
	public Transform spawnpoint;
	public int numberOfWaves;
	public int currentWave;
	public bool isCurrentWaveAlive = false;

	public List<MonsterData> monsterListWave = new List<MonsterData>();

	//Vector3 spawnpoint = new Vector3 (58, 1, 45);
	public List<GameObject> enemies = new List<GameObject>();

	void Start () 
	{
		for (int i = 0; i < 20; i++) {
			//Debug.Log (monsterListWave [0].enemyObject [Random.Range (0, monsterListWave [0].enemyObject.Length)].gameObject.name);
		}

	}
	

	void Update () 
	{
		ClearList ();
		CheckWave ();
		if (!isCurrentWaveAlive) 
		{
			isCurrentWaveAlive = true;
			currentWave++;
			StartCoroutine (Spawn());
		}
	}

	void SpawnEnemyType(GameObject type, MonsterType enemyType)
	{
		GameObject enemy = (GameObject)Instantiate (type, spawnpoint);
		enemies.Add (enemy);
		enemy.GetComponent<EnemyStats> ().monsterType = enemyType;
		if (enemyType == MonsterType.Ground || enemyType == MonsterType.Flying) 
		{
			enemy.GetComponent<EnemyStats> ().maxHP = 100 + (currentWave * 30);
			enemy.GetComponent<EnemyStats> ().hp = 100 + (currentWave * 30);
			enemy.GetComponent<EnemyStats> ().goldValue = 10 + (currentWave * 1);
		} 
		else 
		{
			enemy.GetComponent<EnemyStats> ().maxHP = (currentWave * 5000);
			enemy.GetComponent<EnemyStats> ().hp = (currentWave * 5000);
			enemy.GetComponent<EnemyStats> ().goldValue = (currentWave * 3000);
		}
	}

	IEnumerator Spawn()
	{
		for (int i = 0; i < 2 + currentWave * 3; i++) 
		{
			if (currentWave % 2 == 0)
				SpawnEnemyType (crow, MonsterType.Flying);
			else
				SpawnEnemyType (goblin, MonsterType.Ground);
			yield return new WaitForSeconds (0.01f);
		}
		if (currentWave % 5 == 0)
			SpawnEnemyType (dragon, MonsterType.Flying_Boss);
	}

	void CheckWave()
	{
		if (enemies.Count <= 0) 
			isCurrentWaveAlive = false;	
		
	}

	void ClearList()
	{
		for (int i = 0; i < enemies.Count; i++)
			if (enemies [i] == null)
				enemies.Remove (enemies [i]);
	}
}
