﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class SpellArchetype : MonoBehaviour 
{
	public enum SpellType
	{
		Targeted,
		Global,
		Passive,
	}

	[SerializeField]GameObject Meteor;
	protected Collider[] enemies;
	protected Vector3 targetPos;
	Ray ray;
	RaycastHit hit;
	private bool spellIsSelected = false;
	private bool spellOnCoolDown = false;

	protected class SpellProperties
	{
		public float spellCoolDownTime;
		public float spellRange;
		public float spellActivationTime = 0;
		public SpellType eType;
	}

	protected SpellProperties spellProperties = new  SpellProperties ();

	protected abstract void SpellFunction ();

	public void UseSpell()
	{
		spellIsSelected = true;
	}

	protected void TargetAllEnemies()
	{
		enemies = Physics.OverlapSphere (targetPos, Mathf.Infinity, 1 << 8);
	}

	protected void TargetArea()
	{
		enemies = Physics.OverlapSphere (targetPos, spellProperties.spellRange, 1 << 8);
	}

	void Update()
	{
		switch (spellProperties.eType) 
		{
		case SpellType.Targeted:
			if (Input.GetMouseButtonDown (0) && !spellOnCoolDown && spellIsSelected && !EventSystem.current.IsPointerOverGameObject()) 
			{
				spellIsSelected = false;
				GetTargetPoint ();
				TargetArea ();
				if (spellProperties.eType == SpellType.Targeted) 
					Instantiate (Meteor, targetPos, new Quaternion(0,0,0,0));
				StartCoroutine (ActivateSpell ());
			}
			else if (Input.GetMouseButtonDown (0))
				spellIsSelected = false;
			break;
		case SpellType.Global:
			if (!spellOnCoolDown && spellIsSelected) 
			{
				spellIsSelected = false;
				TargetAllEnemies ();
				StartCoroutine (ActivateSpell ());
			}	
			break;
		case SpellType.Passive:
			if (!spellOnCoolDown) 
			{
				TargetAllEnemies ();
				StartCoroutine (ActivateSpell ());
			}
			break;
		}
	}

	void GetTargetPoint()
	{
		ray = Camera.main.ScreenPointToRay (Input.mousePosition); 
		if (Physics.Raycast (ray, out hit)) 
			targetPos = hit.point;
		Debug.Log (targetPos);
	}

	IEnumerator ActivateSpell()
	{
		if (!spellOnCoolDown) 
		{
			StartCoroutine (StartSpellCoolDown());
			yield return new WaitForSeconds (spellProperties.spellActivationTime);
			SpellFunction ();
		}
	}

	IEnumerator StartSpellCoolDown()
	{
		spellOnCoolDown = true;
		yield return new WaitForSeconds (spellProperties.spellCoolDownTime);
		spellOnCoolDown = false;
	}
}
