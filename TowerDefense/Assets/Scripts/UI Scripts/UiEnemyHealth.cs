﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiEnemyHealth : MonoBehaviour {

	[SerializeField]EnemyStats enemyStats;
	[SerializeField]Image image;

	void Start () 
	{
		//image = gameObject.GetComponentInChildren<Image> ();
		image = gameObject.GetComponent<Image>();
		enemyStats = GetComponentInParent<EnemyStats> ();
		//enemyStats = gameObject.GetComponent (typeof(EnemyStats)) as EnemyStats;
	}

	void Update () 
	{
		image.fillAmount = (float)(enemyStats.hp)/(float)(enemyStats.maxHP);
		float a = (float)(enemyStats.hp) / (float)(enemyStats.maxHP);
	}

}
