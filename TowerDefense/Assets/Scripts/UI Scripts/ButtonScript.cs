﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonScript : MonoBehaviour {

	public float coolDowntime = 5;
	float timer = 0;
	Image image;
	public bool onCooldown = false;
	bool isTargeted = false;
	// Use this for initialization
	void Start () 
	{
		image = gameObject.GetComponent<Image> ();
		timer = coolDowntime;
	}
		
	public void Use()
	{
		onCooldown = true;
		this.GetComponent<Button> ().interactable = false;
		isTargeted = false;
	}

	public void CancelUse()
	{
		isTargeted = false;
	}

	public void UseTargeted()
	{
		isTargeted = true;
	}

	void Update () 
	{
		if (Input.GetMouseButtonDown (0) && isTargeted && !EventSystem.current.IsPointerOverGameObject())
			Use ();
		else if (Input.GetMouseButtonDown (1))
			CancelUse ();
			
		if (onCooldown) 
		{
			timer -= Time.deltaTime;
			image.fillAmount = Mathf.Abs((timer/coolDowntime)-1);
			if (timer <= 0) 
			{
				this.GetComponent<Button> ().interactable = true;
				onCooldown = false;
				timer = coolDowntime;
			}
		}
	}
}
