﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatRetriever : MonoBehaviour 
{


	[SerializeField]TowerUpgrade towerUpgrade;
	[SerializeField]public GameObject tower;
	public GameObject ui;
	public Text damage;
	public Text range;
	public Text fireRate;
	Text a;
	Text b;
	Text c;

	void Start () 
	{
		towerUpgrade = gameObject.GetComponent<TowerUpgrade>();
		a = damage.GetComponent<Text> ();
		b = range.GetComponent<Text> ();
		c = fireRate.GetComponent<Text> ();
	}

	// Update is called once per frame
	void Update ()
	{
		tower = towerUpgrade.tower;
		if (tower != null)
		{
			ui.SetActive (true);
			a.text = "Damage: "+ tower.GetComponent<TowerArchetype> ().damage;
			b.text = "Range: " + tower.GetComponent<TowerArchetype> ().towerRange;
			c.text = "Fire rate: " + tower.GetComponent<TowerArchetype> ().fireRate;
		} 
		else
			ui.SetActive (false);
		
	}
	
}
