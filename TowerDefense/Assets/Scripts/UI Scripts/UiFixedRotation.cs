﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiFixedRotation : MonoBehaviour {

	Quaternion fixedRotation;
	void Start () 
	{
		fixedRotation = gameObject.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () 
	{
		gameObject.transform.rotation = fixedRotation;
	}
}
