﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ProjectorScript : MonoBehaviour {

	Ray ray;
	RaycastHit hit;
	Vector3 targetPos;
	// Use this for initialization
	void Start () 
	{
		
	}

	void getTargetPoint()
	{
		ray = Camera.main.ScreenPointToRay (Input.mousePosition); 
		if (Physics.Raycast (ray, out hit)) 
			targetPos = hit.point;
		targetPos = new Vector3 (targetPos.x, 20, targetPos.z);
		this.transform.position = targetPos;
		if(Input.GetMouseButtonDown(0))
			gameObject.SetActive(false);
		else if(Input.GetMouseButtonDown(1))
			gameObject.SetActive(false);
	}

	void Update () 
	{
		getTargetPoint ();
	}
}
