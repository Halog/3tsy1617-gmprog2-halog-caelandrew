﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMeshChanger : MonoBehaviour {

	BulletScript bulletScript;
	[SerializeField]GameObject[]bulletMesh;

	void Start () 
	{
		bulletScript = gameObject.GetComponent<BulletScript> ();
	}


	void Update () 
	{
		if (bulletScript.eType == BulletScript.bulletType.Normal)
			bulletMesh [0].SetActive (true);
		else
			bulletMesh [0].SetActive (false);
		if (bulletScript.eType != BulletScript.bulletType.Normal)
			bulletMesh [1].SetActive (true);
		else
			bulletMesh [1].SetActive (false);
	}
}
