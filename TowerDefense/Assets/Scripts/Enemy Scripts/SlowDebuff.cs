﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SlowDebuff : MonoBehaviour {

	NavMeshAgent agent;
	SlowDebuff checkForDuplicate;

	void Start () 
	{
		agent = gameObject.GetComponent<NavMeshAgent> ();
		checkForDuplicate = gameObject.GetComponent<SlowDebuff> ();
		if (checkForDuplicate != null && checkForDuplicate != this) 
		{
			Destroy (checkForDuplicate);
		}
		StartCoroutine (Slow ());
	}

	IEnumerator Slow()
	{
		agent.speed = 1f;
		yield return new WaitForSeconds (3.0f);
		agent.speed = 3f;
		Destroy (this);
	}
}
