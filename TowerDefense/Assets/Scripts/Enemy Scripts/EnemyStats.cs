﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyStats : MonoBehaviour 
{
	public int maxHP;
	public int hp;
	public int goldValue;
	public ResourceManager manager;
	public MonsterType monsterType;
	public EnemyStats enemyStats;
	void Awake()
	{
		manager = GameObject.Find("BuildManager").GetComponent<ResourceManager>();
		enemyStats = GetComponent<EnemyStats> ();
	}

	public void TakeDamage(int damage)
	{
		this.hp -= damage;
	}

	public bool isDead()
	{
		if (this.hp <= 0) 
		{
			manager.AddGold (goldValue);
			return true;
		}
		else
			return false;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "EnemyGoal") 
		{
			if (enemyStats.monsterType == MonsterType.Flying_Boss)
				manager.SubtractLife (10);
			else
				manager.SubtractLife (1);
			Destroy (gameObject);
		}
	}
}
