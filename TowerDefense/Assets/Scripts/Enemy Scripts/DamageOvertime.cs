﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOvertime : MonoBehaviour {

	EnemyStats enemyStats;
	DamageOvertime checkForDuplicate;

	void Start () 
	{
		enemyStats = this.GetComponent<EnemyStats> ();
		checkForDuplicate = gameObject.GetComponent<DamageOvertime> ();
		if (checkForDuplicate != null && checkForDuplicate != this) 
		{
			Destroy (checkForDuplicate);
		}
		StartCoroutine (Burn ());
	}

	IEnumerator Burn()
	{
		for (int i = 0; i < 12; i++) 
		{
			enemyStats.TakeDamage (2);
			yield return new WaitForSeconds (0.25f);
		}
		Destroy (this);
	}

}
