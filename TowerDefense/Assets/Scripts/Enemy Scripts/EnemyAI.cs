﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour {

	//public Transform target;
	private GameObject target;
	//public GameObject waypoints;
	NavMeshAgent agent;
	EnemyStats enemyStats;
	private Transform[] targetPositions;
	public int currentWayPoint = 0;

	void Start () 
	{
		agent = GetComponent<NavMeshAgent> ();
		enemyStats = GetComponent<EnemyStats> ();
		GetWayPoints ();
		//agent.SetDestination (targetPositions[currentWayPoint].position);
		target = GameObject.Find ("GoalPoint");
	}
	
	// Update is called once per frame
	void Update () 
	{
		//UpdateDestination();
		agent.SetDestination(target.transform.position);
		if (enemyStats.isDead())
			Destroy (gameObject);
	}

	void UpdateDestination()
	{
		if (this.transform.position == targetPositions[currentWayPoint].position) 
		{
			currentWayPoint++;
			agent.SetDestination (targetPositions[currentWayPoint].position);
		}
	}

	void GetWayPoints()
	{
		//targetPositions = waypoints.GetComponentsInChildren<Transform>();
	}


}
