﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class BuildManager : MonoBehaviour {

	Ray ray;
	RaycastHit hit;
	[SerializeField] GameObject manager;
	[SerializeField] GameObject[] prefabTowers;
	[SerializeField] GameObject objectTower;

	void Start () 
	{
	}

	void Update () {
		SelectArea();
	}

	public void CreateTower(int i)
	{
		if (objectTower == null)
		{
			GameObject twr = Instantiate (prefabTowers[i]) as GameObject;
			twr.name = "TestTower";
			objectTower = twr;
		}

	}

	Vector3 SnapToGrid(Vector3 towerObject)
	{
		return new Vector3(Mathf.Round(towerObject.x),
							towerObject.y,
							Mathf.Round(towerObject.z));

	}

	void SelectArea() 
	{

		if (objectTower != null) 
		{	
			ray = Camera.main.ScreenPointToRay (Input.mousePosition); 
			if (Physics.Raycast (ray, out hit, Mathf.Infinity, 1<<11)) {
				Vector3 towerPos = hit.point;
				objectTower.transform.position = SnapToGrid(towerPos);
				//Debug.Log (hit.point);

				if (hit.point.y > 2 && hit.point.y < 3) 
				{
					objectTower.GetComponent<TowerScript> ().Buildable ();
					ResourceManager gold = this.GetComponent<ResourceManager> ();
					if (Input.GetMouseButtonUp (0) && !EventSystem.current.IsPointerOverGameObject() && gold.playerGold > 199) 
					{
						objectTower.GetComponent<TowerScript> ().Build ();
						objectTower.GetComponent<TowerArchetype> ().PlaceTower ();
						objectTower = null;

						gold.playerGold -= 200;
					}
				}
				else
					objectTower.GetComponent<TowerScript> ().NonBuildable ();

				Debug.DrawLine (ray.origin, hit.point, Color.red);
			}
		}
	}
}
