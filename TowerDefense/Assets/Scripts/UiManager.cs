﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour {

	public GameObject Manager;
	Text gold;
	ResourceManager a;

	void Start () 
	{
		gold = GetComponent<Text>();
		a = Manager.GetComponent<ResourceManager> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		gold.text = "" + a.playerGold;
	}
}
