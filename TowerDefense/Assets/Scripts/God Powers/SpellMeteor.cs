﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellMeteor : SpellArchetype 
{

	void Start()
	{
		spellProperties.eType = SpellArchetype.SpellType.Targeted;
		spellProperties.spellCoolDownTime = 14f;
		spellProperties.spellRange = 8f;
		spellProperties.spellActivationTime = 1f;
	}

	protected override void SpellFunction()
	{

		for (int i = 0; i < enemies.Length; i++) 
		{
			EnemyStats enemyStats = enemies [i].GetComponent<EnemyStats> ();
			if (enemyStats != null) 
			{
				switch (enemyStats.monsterType) 
				{
				case MonsterType.Ground:
					enemyStats.TakeDamage ((int)(enemyStats.maxHP * 0.85f));
					break;
				case MonsterType.Flying:
					enemyStats.TakeDamage ((int)(enemyStats.maxHP * 0.85f));
					break;
				case MonsterType.Ground_Boss:
					enemyStats.TakeDamage ((int)(enemyStats.maxHP * 0.10f));
					break;
				case MonsterType.Flying_Boss:
					enemyStats.TakeDamage ((int)(enemyStats.maxHP * 0.10f));
					break;
				}
			}
		}
	}
}
