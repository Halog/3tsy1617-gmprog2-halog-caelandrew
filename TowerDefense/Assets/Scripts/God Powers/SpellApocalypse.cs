﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellApocalypse : SpellArchetype {

	void Start () 
	{
		spellProperties.spellCoolDownTime = 60f;
		spellProperties.eType = SpellArchetype.SpellType.Global;
	}
	
	protected override void SpellFunction ()
	{
		for(int i = 0; i < enemies.Length; i++)
		{
			if (enemies [i] != null) 
			{
				EnemyStats enemyStats = enemies [i].GetComponent<EnemyStats> ();

				switch (enemyStats.monsterType) 
				{
				case MonsterType.Ground:
					enemyStats.TakeDamage (enemyStats.maxHP);
					break;
				case MonsterType.Flying:
					enemyStats.TakeDamage (enemyStats.maxHP);
					break;
				}
			}
		}
	}
}
