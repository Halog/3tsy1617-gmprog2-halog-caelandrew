﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellReaperScythe : SpellArchetype{

	EnemyStats enemyStats;

	void Start () 
	{
		spellProperties.spellCoolDownTime = 5f;
		spellProperties.eType = SpellArchetype.SpellType.Passive;
	}

	protected override void SpellFunction ()
	{
		int damage;
		int rand = Random.Range (0, enemies.Length - 1);
		if (enemies.Length > 0) 
		{
			enemyStats = enemies [rand].GetComponent<EnemyStats> ();
			if (enemyStats.hp <= enemyStats.maxHP * 0.15f) 
			{
				enemies = Physics.OverlapSphere (enemyStats.transform.position, 3.0f, 1<<8);
				damage = (int)(enemyStats.hp * 0.25f);
				enemyStats.TakeDamage (enemyStats.maxHP);
				for (int i = 0; i < enemies.Length; i++) 
				{
					enemyStats = enemies [i].GetComponent<EnemyStats> ();
					enemyStats.TakeDamage (damage);
					Debug.Log (damage);
				}
			}
		}
	}
}
