﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

	public enum bulletType
	{
		Normal,
		Explosive,
		Fire,
		Ice,
	}
	//Arrays
	Transform target;
	Collider[] enemy = null;
	//Bullet Stats
	public int damage = 0;
	public float splashRadius = 2.5f;
	public float projectileSpeed = 10;
	[SerializeField]public bulletType eType;

	void Update () 
	{
		if (target != null)
			BulletTravel ();
		else
			Destroy (gameObject);
	}

	public void SetStats(int damage, bulletType eType)
	{
		this.damage = damage;
		this.eType = eType;
	}

	public void SetTarget(Transform target)
	{
		this.target = target;
	}

	void BulletTravel()
	{
		transform.position = Vector3.MoveTowards (transform.position, target.position, projectileSpeed * Time.deltaTime);
	}
		
	void AreaDamage()
	{
		enemy = Physics.OverlapSphere (transform.position,  splashRadius, 1<<8);
		for (int i = 0; i < enemy.Length; i++) 
		{
			EnemyStats enemyStats = enemy [i].GetComponent<EnemyStats> ();
			if (enemyStats != null) 
			{
				switch (eType) 
				{
				case bulletType.Explosive:
					if (enemyStats.monsterType == MonsterType.Ground || enemyStats.monsterType ==  MonsterType.Ground_Boss)
						enemyStats.TakeDamage (damage);
					break;
				case bulletType.Fire:
					enemyStats.gameObject.AddComponent<DamageOvertime> ();
					enemyStats.TakeDamage (damage);
					break;
				case bulletType.Ice:
					enemyStats.gameObject.AddComponent<SlowDebuff> ();
					enemyStats.TakeDamage (damage);
					break;
				}
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "test") 
		{
			switch (eType) 
			{
			case bulletType.Normal:
				EnemyStats enemyStats = other.GetComponent<EnemyStats> ();
				enemyStats.TakeDamage (damage);
				Destroy (gameObject);
				break;
			case bulletType.Explosive:
				AreaDamage ();
				Destroy (gameObject);
				break;
			case bulletType.Fire:
				AreaDamage ();
				Destroy (gameObject);
				break;
			case bulletType.Ice:
				AreaDamage ();
				Destroy (gameObject);
				break;
			}
		}
	}

}
