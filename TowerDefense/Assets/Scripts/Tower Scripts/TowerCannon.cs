﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerCannon : TowerArchetype
{
	
	void Start()
	{
		this.SetTowerProperties ();
	}

	public override void SetTowerProperties()
	{
		damage = 100;
		towerAmmo = BulletScript.bulletType.Explosive;
		towerRange = 10f;
		fireRate = 2.0f;
	}

	public override void StatGrowth()
	{
		damage += 250;
		towerRange += 2f;
		fireRate -= 0.1f;
		towerLevel += 1;
	}

}
