﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerMeshChanger : MonoBehaviour {

	TowerArchetype towerArchetype;
	[SerializeField]GameObject[]towers;

	void Start () 
	{
		towerArchetype = gameObject.GetComponent<TowerArchetype> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (towerArchetype.towerLevel == 1)
			towers [0].SetActive (true);
		else
			towers [0].SetActive (false);
		if (towerArchetype.towerLevel == 2 || towerArchetype.towerLevel == 3)
			towers [1].SetActive (true);
		else
			towers [1].SetActive (false);
		if (towerArchetype.towerLevel >= 4)
			towers [2].SetActive (true);
		else
			towers [2].SetActive (false);
		
	}
}
