﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBallista : TowerArchetype 
{
	
	void Start()
	{
		this.SetTowerProperties ();
	}

	public override void SetTowerProperties()
	{
		damage = 120;
		towerAmmo = BulletScript.bulletType.Normal;
		towerRange = 20f;
		fireRate = 0.5f;
	}

	public override void StatGrowth()
	{
		damage += 100;
		towerRange += 5f;
		fireRate -= 0.1f;
		towerLevel += 1;
	}

}
