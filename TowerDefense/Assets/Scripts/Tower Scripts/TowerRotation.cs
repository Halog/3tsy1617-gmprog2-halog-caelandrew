﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerRotation : MonoBehaviour
{

	Transform target;
	Vector3 direction;
	Quaternion rotation;
	Vector3 rotate;
	TowerArchetype tower;
	Collider[] enemy;

	void Start ()
	{
		tower = gameObject.GetComponent<TowerArchetype> ();
		InvokeRepeating ("GetTarget", 0, 0.1f);
	}

	void Update ()
	{
		Rotate ();
	}

	void GetTarget ()
	{
		enemy = Physics.OverlapSphere (this.transform.position, 20f, 1 << 8);
	}
		
	void Rotate ()
	{
		if (target == null)
			return;
		target = enemy [0].transform;
		direction = target.transform.position - transform.position;
		direction.Normalize ();
		rotation = Quaternion.LookRotation (direction);
		rotate = Quaternion.Lerp (this.rotation, rotation, 1f * Time.deltaTime).eulerAngles;
		transform.rotation = Quaternion.Euler (0, rotate.y, 0);
	}

}
