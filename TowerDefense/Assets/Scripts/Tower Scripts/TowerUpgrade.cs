﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TowerUpgrade : MonoBehaviour {

	Ray ray;
	RaycastHit hit;
	[SerializeField] GameObject manager;
	[SerializeField] public GameObject tower;
	ResourceManager resourceManager;

	void Start()
	{
		resourceManager = gameObject.GetComponent<ResourceManager> ();
	}
	void Update () 
	{
		SelectTower();
	}

	public void UpgradeTower()
	{
		if (tower != null) 
		{
			if (resourceManager.playerGold >= 500) 
			{
				TowerArchetype towerArchetype = tower.GetComponent<TowerArchetype> ();
				resourceManager.playerGold -= 500;
				towerArchetype.StatGrowth ();
			}
		}
	}

	void SelectTower() 
	{
		ray = Camera.main.ScreenPointToRay (Input.mousePosition); 
		if (Physics.Raycast (ray, out hit, Mathf.Infinity, 1 << 10) && Input.GetMouseButtonDown (0)) 
		{
			tower = hit.collider.gameObject;
		} 
		else if (Input.GetMouseButtonDown (0) && !EventSystem.current.IsPointerOverGameObject ()) 
		{
			tower = null;
		}
	}

}
