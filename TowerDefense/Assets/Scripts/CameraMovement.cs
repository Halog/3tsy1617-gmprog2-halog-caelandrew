﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	float mouseX;
	float mouseY;
	public float mouseBoundary = 1;
	public float cameraSpeed = 20;

	void Start () 
	{
		
	}

	// Update is called once per frame
	void LateUpdate () 
	{
		MouseUpdate ();
		CameraBoundary ();
		MoveCamera ();
	}
	void MoveCamera ()
	{
		//Horizontal Movement
		//Left
		if (mouseX <= mouseBoundary)
			this.transform.position = new Vector3 (transform.position.x - cameraSpeed * Time.deltaTime, this.transform.position.y, this.transform.position.z);
		//Right
		if (mouseX >= Screen.width - mouseBoundary)
			this.transform.position = new Vector3 (transform.position.x + cameraSpeed * Time.deltaTime, this.transform.position.y, this.transform.position.z);
		//Vertical Movement
		//Up
		if (mouseY <= mouseBoundary)
			this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, transform.position.z - cameraSpeed * Time.deltaTime);
		//Down
		if (mouseY >= Screen.height - mouseBoundary)
			this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, transform.position.z + cameraSpeed * Time.deltaTime);
	}
	void MouseUpdate()
	{
		mouseX = Input.mousePosition.x;
		mouseY = Input.mousePosition.y;
		//cursorPos = new Vector3 (mouseX, this.transform.position.y, mouseY);
	}
	void CameraBoundary()
	{
		this.transform.position = new Vector3
		(
		Mathf.Clamp (this.transform.position.x, 13, 50),
		this.transform.position.y,
		Mathf.Clamp (this.transform.position.z, 0, 47)
		);
	}
}
